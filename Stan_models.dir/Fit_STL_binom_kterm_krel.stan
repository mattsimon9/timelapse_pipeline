data {
  int NE; // num entries in data frame.
  int NF; // num features/TSSs
  int nDT; // number of drug treatments (or other treatment conditions)
  int TP[NE]; // type for each entry (+s4U = 1, -s4U = 0)
  int DT[NE]; // drug treatment for each entry
  int FE[NE]; //  feature for each entry
  int num_mut[NE]; //  number of mutations in each entry
  int num_obs[NE]; // number of observations
  int num_trial[NE]; // number of trials
}

parameters {
  real logit_p_o_m; // global background mutation rat
  vector[nDT] logit_p_n_m; // global TimeLapse mutation rate
  real<lower=0> logit_p_o_sd; // sd for background mutation rate
  vector<lower=0>[nDT] logit_p_n_sd; // sd for TimeLapse mutation rate
  vector[NF] z_o;  // feature-specific z-score for background mutation rate
  vector[nDT] z_n[NF]; // feature-specific z-score for TimeLapse mutation rate per treatment
  
  vector<lower=0>[2] rate_UT[NF]; // Parameter for the rate in uninhibited samples
  vector<lower=0>[2] f_fp[NF]; // Parameter for expontial which determines rate +FP
  
 }

transformed parameters {
  
  vector[nDT] logit_p_n[NF];  // feature-specific TimeLapse mutation rate per treatment
  vector[NF] logit_p_o; // feature-specific background mutation rate
  vector[2] rate_FP[NF]; // turnover rate under FP inhibition
  vector[nDT] frac_new[NF]; // Inferred fraction new
  
  for (i in 1:NF) {
    for (j in 1:2) {
      rate_FP[i,j] = rate_UT[i,j]/exp(f_fp[i,j]);
    }
    for (j in 1:nDT) {
      logit_p_n[i, j] = logit_p_n_m[j] + z_n[i, j]*logit_p_n_sd[j];
    }
    frac_new[i, 1] = 1-exp(-rate_UT[i,1]*5);
    frac_new[i, 2] = 1-exp(-rate_FP[i,1]*5);
    frac_new[i, 3] = 1-exp(-rate_UT[i,2]*5);
    frac_new[i, 4] = 1-exp(-rate_FP[i,2]*5);
    logit_p_o[i] = logit_p_o_m + z_o[i]*logit_p_o_sd;
  }
  
}

model {
  // Priors:
  logit_p_o_m ~ normal(-6, 0.5);
  logit_p_n_m ~ normal(-2.5, 0.5);
  logit_p_o_sd ~ cauchy(0, 1);
  logit_p_n_sd ~ cauchy(0, 1);
  
  z_o ~ normal(0,1);
  
  for (i in 1:NF) {
    for (j in 1:2) {
      z_n[i, j] ~ normal(0, 1);
      f_fp[i,j] ~ normal(0,2);
      rate_UT[i,j] ~ gamma(0.5, 1.75);
    }
  }

    // Model fraction of reads that are new.
    for (i in 1:NE) {
        target += num_obs[i] * log_mix(TP[i] * frac_new[FE[i], DT[i]],
          binomial_logit_lpmf(num_mut[i] | num_trial[i], logit_p_n[FE[i], DT[i]]),
          binomial_logit_lpmf(num_mut[i] | num_trial[i], logit_p_o[FE[i]]));
    }
    
 }
 
generated quantities {

 vector[NF] L2FC_obs;
 vector[2] rel_rate[NF];
 vector[NF] L2FC_term;
 vector[NF] L2FC_rel;
 vector[NF] delta_L2FC;
 vector[NF] logodds;
  
 for (i in 1:NF) {
   L2FC_obs[i] = log2(rate_UT[i,2]/rate_UT[i,1]);
   rel_rate[i,1] = rate_UT[i,1] - rate_FP[i,1];
   rel_rate[i,2] = rate_UT[i,2] - rate_FP[i,2];
   L2FC_term[i] = log2(rate_FP[i,2]/rate_FP[i,1]);
   L2FC_rel[i] = log2(rel_rate[i,2]/rel_rate[i,1]);
   delta_L2FC[i] = fabs(L2FC_rel[i]) - fabs(L2FC_term[i]);
   logodds[i] = log2(rel_rate[i,1]/rate_FP[i,1]);
 }
 
}

