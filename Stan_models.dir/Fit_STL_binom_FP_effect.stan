data {
  int NE; // num entries in data frame.
  int NF; // num features/TSSs
  int nDT; // number of drug treatments (or other treatment conditions)
  int TP[NE]; // type for each entry (+s4U = 1, -s4U = 0)
  int DT[NE]; // drug treatment for each entry
  int FE[NE]; //  feature for each entry
  int num_mut[NE]; //  number of mutations in each entry
  int num_obs[NE]; // number of observations
  int num_trial[NE]; // number of trials
}

parameters {
  real logit_p_o_m; // mean background mutations
  vector[nDT] logit_p_eff_m; // mean new mutations
  real<lower=0> logit_p_o_sd; // sd for background mutations
  vector<lower=0>[nDT] logit_p_eff_sd; // sd for timelapse mutations
  vector[NF] z_o;  // feature-specific z-score for background mutation rate
  vector[nDT] z_n[NF]; // feature-specific z-score for TimeLapse mutation rate per treatment
  
  vector<lower=0>[NF] rate; // Parameter for the steady state rate
  
  real eff_g; //  Global parameter for effect of FP treatment
  real<lower=0> eff_sd; //  Parameter for standard deviation of global effect of FP treatment
  vector[NF] eff; //  Parameter for effect of FP treatment per TSS
  
 }

transformed parameters {
  
  vector[nDT] logit_p_eff[NF];  // feature-specific TimeLapse mutation rate per treatment.
  vector[nDT] logit_p_n[NF];  // feature-specific TimeLapse mutation rate.
  vector[NF] logit_p_o; // feature-specific background mutation rate
  vector[nDT] frac_new[NF]; // Inferred fraction new
  
  for (i in 1:NF) {
    logit_p_o[i] = logit_p_o_m + z_o[i]*logit_p_o_sd;
    for (j in 1:nDT) {
      logit_p_eff[i,j] = logit_p_eff_m[j] + z_n[i, j]*logit_p_eff_sd[j];
      logit_p_n[i,j] = logit_p_eff[i,j] + logit_p_o[i];
      }
    frac_new[i,1] = 1-exp(-rate[i]*5);
    frac_new[i,2] = 1-exp(-rate[i]*exp(eff[i])*5);
    }
  
}

model {
  // Priors:
  logit_p_o_m ~ normal(-6, 0.5);
  logit_p_eff_m ~ normal(4, 0.5);
  logit_p_o_sd ~ normal(0, 1);
  logit_p_eff_sd ~ normal(0, 1);
  
  z_o ~ normal(0,1);
  
  rate ~ gamma(0.5, 1.75);
  
  eff_g ~ normal(0, 1); //FP effect size global prior
  eff_sd ~ normal(0, 1); //FP effect size SD prior
  eff ~ normal(eff_g, eff_sd); //FP effect size prior for individual TSS
  
  for (i in 1:NF) {
    for (j in 1:nDT) {
      z_n[i,j] ~ normal(0, 1);
    }
  }

    // Model fraction of reads that are new.
    for (i in 1:NE) {
        target += num_obs[i] * log_mix(TP[i] * frac_new[FE[i], DT[i]],
          binomial_logit_lpmf(num_mut[i] | num_trial[i], logit_p_n[FE[i], DT[i]]),
          binomial_logit_lpmf(num_mut[i] | num_trial[i], logit_p_o[FE[i]]));
    }
    
 }
 
generated quantities {

 vector[NF] L2FC;
 vector[NF] rate_FP; // rate under FP inhibition
  
 for (i in 1:NF) {
   rate_FP[i] = rate[i]*exp(eff[i]);
   L2FC[i] = log2(rate_FP[i]/rate[i]);
 }
 
}

