#!/bin/bash

# Source the paths and variables:
    source $1
    sample=$2

# load tools
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi


    cd $MASTER_DIR/"$sample".dir      

# Spread the work load so all cpus are working at all times   
    declare $(samtools view -@ "$cpus" ../"$sample"_tl.bam \
    			| wc -l \
    			| awk -v fragment_size="$fragment_size" \
                      -v cpus="$cpus" '{
                                        nFrag = int($1 / fragment_size)                         # Calculate to how many fragments must the .bam be split
                                        if ($1 % fragment_size != 0) {nFrag = nFrag + 1}        # If there are some left over reads then increase number of fractions

                                        nBatchRun = int(nFrag / cpus)                           # In how many batches will the fragments be processed
                                        if (nFrag % cpus != 0) { nBatchRun = nBatchRun + 1}     # If there are some leftover fragments that will not fill up all cpus, then incerease number of batches 

                                        newFragmentNumber = nBatchRun * cpus

                                        newFragmentSize = int($1 / newFragmentNumber)
                                        if ($1 % newFragmentNumber != 0) {newFragmentSize = newFragmentSize + 2}    # if there are still some reads leftover, then increase fragment size by 1 read-pair

                                        print "newFragmentNumber="newFragmentNumber
                                        print "newFragmentSize="newFragmentSize
                                    }')

    echo "* The number of fragments for sample $sample is $newFragmentNumber"                                    
    echo "* The fragment size is set to $newFragmentSize alignments per fragment"



    for f in $(seq 1 $newFragmentNumber); do
        samtools view -H ../"$sample"_tl.bam > "$f"_"$sample".sam
    done &&


    samtools view ../"$sample"_tl.bam \
    	| awk \
    		-v fragment_size="$newFragmentSize" \
    		-v sample="$sample" \
    		-f $FRAGMENT_AWK &&

    for f in $(seq 1 $newFragmentNumber); do
        samtools view -@ "$cpus" -o "$f"_"$sample"_frag.bam "$f"_"$sample".sam
        rm "$f"_"$sample".sam
    done &&

    echo "* Aligned .sam file fragmented for sample $sample"

# move bam fragments to master folder
    mv *_frag.bam $MASTER_DIR/.

# remove any remaining sam files:
    if [[ "$cleanup" = "TRUE" ]]; then
        rm -f *.sam
    fi

