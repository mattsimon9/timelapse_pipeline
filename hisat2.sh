#!/bin/bash

# Source the paths and variables:
    source $1
    sample=$2

    cd "$MASTER_DIR"/"$sample".dir


# align trimmed reads to the  genome
    if [[ -n $HISAT2_MOD ]]; then module load ${HISAT2_MOD}; fi
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi

    echo "* Aligning reads with HISAT2 for sample $sample"

    if [[ "$chr_tag" = "TRUE" ]]; then
        echo "* chr tag will be included in alignment for " $sample
    fi


    if [[ "$READS" = "FR" ]]; then
       $HISAT2 \
            -p "$cpus" \
            -x $HISAT_INDEX \
            -1 "$sample".t.r1.fastq \
            -2 "$sample".t.r2.fastq \
            $( if [ $chr_tag = 'TRUE' ]; then echo "--add-chrname "; fi ) \
            --rna-strandness FR \
            --mp 4,2 \
            -S "$sample".sam
    elif [[ "$READS" = "RF" ]]; then
        $HISAT2 \
            -p "$cpus" \
            -x $HISAT_INDEX \
            -1 "$sample".t.r2.fastq \
            -2 "$sample".t.r1.fastq \
            $( if [ $chr_tag = 'TRUE' ]; then echo "--add-chrname "; fi ) \
            --rna-strandness FR \
            --mp 4,2 \
            -S "$sample".sam
    elif [[ "$READS" = "F" ]]; then
        $HISAT2 \
            -p "$cpus" \
            -x $HISAT_INDEX \
            -U "$sample".t.fastq \
            $( if [ $chr_tag = 'TRUE' ]; then echo "--add-chrname "; fi ) \
            --mp 4,2 \
            -S "$sample".sam
    else
        echo "! No FR/RF/F method recognized for " $sample
        exit 1
    fi &&


    if [[ "$FORMAT" = "SE" ]]; then
        rm "$sample".t.fastq
    else
        rm "$sample".t.r[12].fastq
    fi


    $SAMTOOLS view -@ "$cpus" -o "$sample"Aligned.out.bam "$sample".sam

    rm "$sample".sam

    echo "* Alignment script finished for " $sample
