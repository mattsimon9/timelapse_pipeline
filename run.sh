#!/bin/bash
# This is a wrapper script that allows running TimeLapse pipeline without need to change email information in runscript.sh
#
# Run this script as ./run.sh confing.sh
# Alternatively, you can run as ./run.sh -s sample1[,sample2...] -c control1[,control2...] -i inDir -o outDir [params...] [<confing file>]


function PrintUsage {
    echo "Pipeline for analyzing TimeLapse data

Usage: run.sh <config file>
       -or-
       run.sh -s <sample1>[,<sample2>...] -c <control1>[,<control2>...] -i <inDir> -o <outDir> [params...] [<config file>]
    "

    echo "parameters: <config file>            - path to configuration file      
            -s|--samples              - comma-separated list of all sample and control names
            -c|--controls             - comma-separated list of control name(s)
            -i|--inputDir             - directory with .fastq (.fastq.gz) files
            -o|--outputDir            - directory for writing results
            -x|--prefix               - avoid typing repetitive part of sample names e.g. \"Sample_*\" (default: none)
            -g|--genome               - species [Hs, Mm, Dm]  (default: Mm)
            -f|--format               - reads format [PE, SE, NU]  (default: PE)
            -r|--reads                - reads strandness [FR, RF, F]  (default: RF)
            -m|--mutation             - Mutation type(s) to analyze [TC, GA, TC,GA] (default: TC)
            -b|--3base                - use HISAT-3N or Bismark aligner (default: off)
            -t|--stl                  - experiment is STL (default: off)
            -u|--mutpos               - call mutations by absolute genomic location (default: off)
            -w|--bigwig               - Make additional .bigWig track files (default: off)
            -a|--addchr               - add chr prefix to chromosome names during alignment (default: off)
            -n|--spikein              - common string identifying spikein names in .gtf file
            --fasta                   - path to genome fasta file
            --annotation              - path to gtf genome annotation file
            --genome_index            - path to aligner genome index (HISAT-3N, HISAT, Bowtie2, Bismark)
            -p|--parallel             - number of cpus used by pipeline or Slurm jobs [int] (default: 20)
            -e|--mem                  - memory used by Slurm jobs [intG](default: 119G)
            -l|--slurm                - use Slurm for job scheduling (default: off)
            -j|--jobname              - name for Slurm job
            -@|--email                - email address for Slurm notifications
            -h|--help|-?              - this message 
    "

    echo "Sequencing .fastq(.gz) files for every sample must be in a separate sub-directory.
Sub-directory name must match the sample name.

    ../inDir ┬─ Sample1 ┬─ reads_R1.fastq
             │          ├─ reads_R2.fastq
             │          ┊
             ├─ Sample2 ┬─ reads_R1.fastq
             │          ├─ reads_R2.fastq
             ┊          ┊                 
    "

    echo "Before first run fill out required system paths for tools in config.sh file."
    exit
}

# Set defaul values
samples=
control_samples=
LINK_BASE=
MASTER_DIR=
prefix=
SPECIES="Mm"
FORMAT="PE"
READS="RF"
mut_tracks="TC"
three_base="FALSE"
STL="FALSE"
mut_pos="FALSE"
chr_tag="FALSE"
cpus=20
mem=119G
SLURM="FALSE"
JOB_NAME="TimeLapse"
email=
spikein_name=
bigwig="FALSE"

GIT_PATH=$( cd "$( dirname "$0" )" >/dev/null 2>&1 && pwd )

# Check if parameters are set
    if [[ $# -eq 0 ]]; then

        PrintUsage

    elif [[ $# -eq 1 ]] && [ ! "$1" = "--help" ] && [ ! "$1" = "-h" ] && [ ! "$1" = "-?" ]; then


        set --  "$( cd "$( dirname "$1" )" >/dev/null 2>&1 && pwd )/$(basename "$1")"  # Get absolute path to config file in case a relative path was provided
        #set -- "$(readlink -e $1)"      # macOS incompatible solution

        if [ ! -f "$1" ]; then                # Check if confing.sh file exists at provided path
            echo '!!! Configuration file not found at provided path'
            PrintUsage
        else
            source $1
        fi

    else

        # Read parameters
        POSITIONAL=()
        while [[ $# -gt 0 ]]; do
            case $1 in

                -s|--samples)
                    samples=$(echo "$2" | sed 's/,/ /g')
                    shift # past argument
                    shift # past value
                ;;
                -c|--controls)
                    control_samples=$(echo "$2" | sed 's/,/ /g')
                    shift
                    shift
                ;;
                -i|--inputDir)
                    LINK_BASE="$( cd "$( dirname "$2" )" >/dev/null 2>&1 && pwd )/$(basename "$2")" # Get absolute path if relative was provided 
                    shift
                    shift
                ;;
                -o|--outputDir)
                    MASTER_DIR="$( cd "$( dirname "$2" )" >/dev/null 2>&1 && pwd )/$(basename "$2")"
                    shift
                    shift
                ;;
                -x|--prefix)
                    prefix="$2"
                    shift
                    shift
                ;;
                -g|--genome)
                    SPECIES=$2
                    shift
                    shift
                ;;
                -f|--format)
                    if [[ $2 = "PE" ]] || [[ $2 = "SE" ]] || [[ $2 = "NU" ]]; then FORMAT="$2"
                    else
                        echo '!!! Format of reads must be PE, SE or NU'
                        PrintUsage
                    fi
                    shift
                    shift
                ;;
                -r|--reads)
                    if [[ $2 = "FR" ]] || [[ $2 = "RF" ]] || [[ $2 = "F" ]]; then READS="$2"
                    else
                        echo '!!! Read strandness must be FR, RF or F'
                        PrintUsage
                    fi
                    shift
                    shift
                ;;
                -m|--mutation)
                    if [[ $2 = "TC" ]] || [[ $2 = "GA" ]] || [[ $2 = "TC,GA" ]]; then mut_tracks="$2"
                    else
                        echo '!!! Mutation type must be TC, GA or TC,GA'
                        PrintUsage
                    fi
                    shift
                    shift
                ;;
                -b|--3base)
                    three_base="TRUE"
                    shift
                ;;
                -t|--STL)
                    STL="TRUE"
                    shift
                ;;
                -u|--mutpos)
                    mut_pos="TRUE"
                    shift
                ;;
                -w|--bigwig)
                    bigwig="TRUE"
                    shift
                ;;
                -a|--addchr)
                    chr_tag="TRUE"
                    shift
                ;;
                -n|--spikein)
                    spikein_name="$2"
                    shift
                    shift
                ;;
                --fasta)
                    fasta="$( cd "$( dirname "$2" )" >/dev/null 2>&1 && pwd )/$(basename "$2")"
                    shift
                    shift
                ;;
                --annotation)
                    annotation="$( cd "$( dirname "$2" )" >/dev/null 2>&1 && pwd )/$(basename "$2")"
                    shift
                    shift
                ;;
                --genome_index)
                    genome_index="$( cd "$( dirname "$2" )" >/dev/null 2>&1 && pwd )/$(basename "$2")"
                    shift
                    shift
                ;;
                -p|--parallel)
                    cpus="$2"
                    shift
                    shift
                ;;
                -e|--mem)
                    mem="$2"
                    shift
                    shift
                ;;
                -l|--slurm)
                    SLURM="TRUE"
                    shift
                ;;
                -j|--jobname)
                    JOB_NAME="$2"
                    shift
                    shift
                ;;
                -@|--email)
                    email="$2"
                    shift
                    shift
                ;;
                -h|--help|-\?)
                    PrintUsage
                ;;
                *)    # unknown option
                    POSITIONAL+=("$1") # save it in an array for later
                    shift
                ;;
            esac
        done
        set -- "${POSITIONAL[@]}" # restore positional parameters


        # Check if all required parameters were set
        if  [ -z "$samples" ] || \
        [ -z "$control_samples" ] || \
        [ -z "$LINK_BASE" ] || \
        [ -z "$MASTER_DIR" ]; then
            echo '!!! Required parameters -s, -c, -i, -o were not provided'
            PrintUsage
        fi

        # In case config file was provided along with paramters
        if [[ $# -eq 1 ]]; then

            set -- "$( cd "$( dirname "$1" )" >/dev/null 2>&1 && pwd )/$(basename "$1")"
            #set -- "$(readlink -e $1)"

            if [ ! -f "$1" ]; then
                echo '!!! Configuration file not found at provided path'
                PrintUsage
            else
                configFile=$1
            fi

        elif [[ $# -eq 0 ]]; then
            configFile="${GIT_PATH}/config.sh"
        else
            echo '!!! Unrecognized parameters provided'
            PrintUsage
        fi


        # Create copy of config file and modify with provided parameters
        currentDate=$(date +"%y%m%d-%H%M%S")

        sed -r \
            "s/(^[[:space:]]*samples=).*/\1($samples)/ ; \
             s/(^[[:space:]]*control_samples=).*/\1($control_samples)/ ; \
             s|(^[[:space:]]*LINK_BASE=).*|\1\'$LINK_BASE\'| ; \
             s|(^[[:space:]]*MASTER_DIR=).*|\1\'$MASTER_DIR\'| ; \
             s/(^[[:space:]]*prefix=).*/\1$prefix/ ; \
             s/(^[[:space:]]*SPECIES=).*/\1$SPECIES/ ; \
             s/(^[[:space:]]*FORMAT=).*/\1$FORMAT/ ; \
             s/(^[[:space:]]*READS=).*/\1$READS/ ; \
             s/(^[[:space:]]*mut_tracks=).*/\1$mut_tracks/ ; \
             s/(^[[:space:]]*bigwig=).*/\1$bigwig/ ; \
             s/(^[[:space:]]*three_base=).*/\1$three_base/ ; \
             s/(^[[:space:]]*STL=).*/\1$STL/ ; \
             s/(^[[:space:]]*mut_pos=).*/\1$mut_pos/ ; \
             s/(^[[:space:]]*chr_tag=).*/\1$chr_tag/ ; \
             s/(^[[:space:]]*spikein_name=).*/\1$spikein_name/ ; \
             s/(^[[:space:]]*cpus=).*/\1$cpus/ ; \
             s/(^[[:space:]]*mem=).*/\1$mem/ ; \
             s/(^[[:space:]]*SLURM=).*/\1$SLURM/ ; \
             s/(^[[:space:]]*JOB_NAME=).*/\1$JOB_NAME/ ; \
             s/(^[[:space:]]*email=).*/\1$email/ ; \
             s|(^[[:space:]]*GIT_PATH=).*|\1\'$GIT_PATH\'|" $configFile > ${GIT_PATH}/config_${currentDate}.sh

             # explanation: match: (any number of tabs/spaces + variable=) + rest of the line
             #              replace with: text matched with pattern inside () + replacement text

        if [ ! -z $fasta ]; then sed -r -i "s|(^[[:space:]]*genome_fasta=).*|\1\'$fasta'|g" ${GIT_PATH}/config_${currentDate}.sh; fi
        if [ ! -z $annotation ]; then sed -r -i "s|(^[[:space:]]*annotation_gtf=).*|\1\'$annotation'|g" ${GIT_PATH}/config_${currentDate}.sh; fi
        if [ ! -z $genome_index ]; then sed -r -i "s|(^[[:space:]]*.+_INDEX=).*|\1\'$genome_index'|g" ${GIT_PATH}/config_${currentDate}.sh; fi
            # Note: does not work on MacOS as it requires -i '' 

        configFile="${GIT_PATH}/config_${currentDate}.sh"
        set -- "$configFile"
        source $1

    fi




# Start pipeline
    if [ "$SLURM" = "TRUE" ]; then
        sbatch --job-name="$JOB_NAME" \
               --output="$JOB_NAME".txt \
               --ntasks=1 \
               --cpus-per-task=1 \
               --time=48:00:00 \
               --mem=1G \
               --mail-type=ALL \
               --mail-user="$email" \
               $GIT_PATH/runscript.sh $1

    else
        $GIT_PATH/runscript.sh $1
    fi
