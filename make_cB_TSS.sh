#!/bin/bash

# Source the paths and variables:
    source $1
    sample=$2

    cd TSScall.dir

    if [[ -n $PIGZ_MOD ]]; then module load ${PIGZ_MOD}; fi
    if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi
    if [[ -n $BEDTOOLS_MOD ]]; then module load ${BEDTOOLS_MOD}; fi
    if [[ -n $PYTHON_MOD ]]; then module load ${PYTHON_MOD}; fi
    if [[ -n $TSSCALL_MOD ]]; then module load ${TSSCALL_MOD}; fi



# Merge all bam files
    $SAMTOOLS merge -@ "$cpus" "$JOB_NAME"_all.bam *_sort.bam

    $BEDTOOLS genomecov -ibam "$JOB_NAME"_all.bam -bg -5 -strand + | awk '{ printf "%s \t %d \t %d \t %d\n", $1,$2,$3,$4 }' > FORWARD_BEDGRAPH.bedgraph &
    $BEDTOOLS genomecov -ibam "$JOB_NAME"_all.bam -bg -5 -strand - | awk '{ printf "%s \t %d \t %d \t %d\n", $1,$2,$3,$4 }' > REVERSE_BEDGRAPH.bedgraph
    wait

# Make .chrom.sizes file from .bam file header
    chrom_sizes=$(echo ${genome_fasta##*/} | cut -f 1 -d '.')".chrom.sizes"
    if [ ! -f $chrom_sizes ]; then
                samtools view -H "$JOB_NAME"_all.bam \
                    | awk -v OFS="\t" ' $1 ~ /^@SQ/ {split($2, chr, ":")
                                                     split($3, size, ":")
                                                     print chr[2], size[2]}' > "$chrom_sizes"
    fi

# Call TSSs with TSScall
    if [[ "$SPECIES" == "Dm" ]]; then
        $PYTHON $TSSCALL \
            -a $annotation_gtf \
            --detail_file "$JOB_NAME"_TSS_detail.bed \
            --annotation_search_window 500 \
            --annotation_join_distance 100 \
            --call_method global \
            FORWARD_BEDGRAPH.bedgraph \
            REVERSE_BEDGRAPH.bedgraph \
            "$chrom_sizes" \
            "$JOB_NAME"_TSS.bed
    else
        $PYTHON $TSSCALL \
            -a $annotation_gtf \
            --detail_file "$JOB_NAME"_TSS_detail.bed \
            --annotation_search_window 1000 \
            --annotation_join_distance 200 \
            --call_method bin_winner \
            FORWARD_BEDGRAPH.bedgraph \
            REVERSE_BEDGRAPH.bedgraph \
            "$chrom_sizes" \
            "$JOB_NAME"_TSS.bed
    fi

# Pair mutation & TSS information for each read
    if [[ "$FORMAT" = "SE" ]]; then
        $GNUPARALLEL -j $cpus "LC_ALL=C join -t ',' \
                                             -j 1 \
                                             -o 2.2,1.2,2.3,2.4,2.5,2.6,2.7,1.3,1.4 \
                                             <($PIGZ -d -k -c -p $cpus ../{1}_counts.csv.gz \
                                                | awk -v FS=',' -v OFS=',' 'NR > 1 {print \$1, \"{1}\", \$24, \$4}' \
                                                | LC_ALL=C sort -t ',' -k 1b,1) \
                                             <($BEDTOOLS window -abam {1}_sort.bam -b ${JOB_NAME}_TSS.bed -w 200 -bed -sm \
                                                | awk -v FS='\t' -v OFS=',' '{print \$4, \$7, \$2, \$3, \$12, \$8, \$10}' \
                                                | LC_ALL=C sort -t ',' -k 1b,1) \
                                    | awk -v OFS=',' -v FS=',' '
                                                {
                                                    ++count[\$0]
                                                }
                                                END {
                                                    for (row in count) {
                                                        print row, count[row]
                                                    }
                                                }' " ::: ${samples[@]} \
            | awk '
                BEGIN {
                    print "rname,sample,pos5,pos3,strand,TSS_start,TSS_name,TC,nT,n"
                }
                {
                    print
                }' \
            | $PIGZ -p $cpus > cB_TSS.csv.gz

    elif [[ "$FORMAT" = "PE" ]]; then
        $GNUPARALLEL -j $cpus "LC_ALL=C join -t ',' \
                                             -j 1 \
                                             -o 2.2,1.2,2.3,2.4,2.5,2.6,2.7,1.3,1.4 \
                                             <($PIGZ -d -k -c -p $cpus ../{1}_counts.csv.gz \
                                                | awk -v FS=',' -v OFS=',' 'NR > 1 {print \$1, \"{1}\", \$24, \$4}' \
                                                | LC_ALL=C sort -t ',' -k 1b,1) \
                                             <($BEDTOOLS bamtobed -i {1}_sort.bam -bedpe \
                                                | awk '{ print \$1 \$4 \$6 \$7 \$8 \$9 }' \
                                                | $BEDTOOLS window -a stdin -b ${JOB_NAME}_TSS.bed -w 200 -bed -sm \
                                                | awk -v FS='\t' -v OFS=',' '{print \$4, \$7, \$2, \$3, \$12, \$8, \$10}' \
                                                | LC_ALL=C sort -t ',' -k 1b,1) \
                                    | awk -v OFS=',' -v FS=',' '
                                                {
                                                    ++count[\$0]
                                                }
                                                END {
                                                    for (row in count) {
                                                        print row, count[row]
                                                    }
                                                }' " ::: ${samples[@]} \
                | awk '
                BEGIN {
                    print "rname,sample,pos5,pos3,strand,TSS_start,TSS_name,TC,nT,n"
                }
                {
                    print
                }' \
            | $PIGZ -p $cpus > cB_TSS.csv.gz
    fi

echo "** Information combined and output files created"




