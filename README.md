# TimeLapse pipeline v0.4
This pipeline is designed for analysis of RNA sequencing results from TimeLapse style experiments and provides information about number of mutations induced by metabolic labeling. Currently it is tested for TC and GA mutations, but it is forward compatible to support analysis of any number and combination of mutation types.

## Setup
This pipeline is intended to be run on HPC clusters and takes advantage of Slurm task scheduler and Lmod module system, but supports running on other systems and local computers with \*nix systems as well. Before first run, you must modify `# System Information` section of `config.sh` or its copy.

* Provide absolute path to required software executables. e.g.`/user/bin/bowtie2/bowtie2` or `bowtie2` if path is in `$PATH`
* If your system uses Lmod module system, provide full names of the modules.

Optionally, you can provide information about the experiment in the `# Experiment Information` section (see below for command line options of `# Experiment Information`).
* Output directory `MASTER_DIR=`
* Input directory with samples' sequencing files `LINK_BASE=`
* Names of all samples including controls e.g. `samples=("JS190101" "JS190102" "JS190103" "JS190104")`
* Specify which are -4SU control samples e.g. `control_samples=("JS190101" "JS190102")`
* Species `SPECIES=`
* Type of mutations to detect. TC or GA or TC,GA `mut_tracks=`
* If reads are pair-end `FORMAT=`
* Strandedness of reads in your sequencing libraries. FR or RF or F for single-end `READS=`
* Provide full paths to genome files specific to your species.
* Provide full path to directory where TimeLapse pipeline is located.

Note: Sequencing .fastq(.gz) files for every sample must be in a separate sub-directory with name matchig the sample name inside an input directory specified in `LINK_BASE=` line.  
```
    ../inDir ┬─ Sample1 ┬─ reads_R1.fastq
             │          ├─ reads_R2.fastq
             │          ┊
             ├─ Sample2 ┬─ reads_R1.fastq
             │          ├─ reads_R2.fastq
             ┊          ┊
```

In addition make sure required packages are installed in your R environment

### Requirements
Pipeline was tested with following versions of software, but it is expected to run with also newer versions.<br>
Required software:
* Bash 4.2.46
* SAMtools 1.12
* HISAT-3N
* HTSeq 0.13.5
* GNU parallel 20210222
* pigz 2.6
* cutadapt 3.2
* BCFtools 1.11
* FastUniq 1.1
* R 4.0.3
* STAR 2.7.7a
* Awk 4.0.2
* python 3.8.6 + pysam 0.16.0.1

Optional:
* HISAT2 2.1.0
* bowtie2 2.4.2
* Bismark 0.23.0
* BEDTools 2.27.1
* Stan
* TSScall

### Required R packages
* tidyverse  
* optparse
* edgeR

## Initializing the pipeline
If you provided information about your experiment in the `# Experiment Information` and `# System Information` sections of the `confing.sh` file you can start the pipeline as: `run.sh <config file>` e.g. `./run.sh path/config.sh`

Alternatively, you can specify these information as parameters: `run.sh -s <sample1>[,<sample2>...] -c <control1>[,<control2>...] -i <inDir> -o <outDir> [params...] [<config file>]`

```
parameters: <config file>             - path to configuration file      
            -s|--samples              - comma-separated list of all sample and control names
            -c|--controls             - comma-separated list of control name(s)
            -i|--inputDir             - directory with .fastq (.fastq.gz) files
            -o|--outputDir            - directory for writing results
            -x|--prefix               - avoid typing repetitive part of sample names e.g. Sample_* (default: none)
            -g|--genome               - species [Hs, Mm, Dm]  (default: Mm)
            -f|--format               - reads format [PE, SE, NU]  (default: PE)
            -r|--reads                - reads strandedness [FR, RF, F]  (default: RF)
            -m|--mutation             - Mutation type(s) to analyze [TC, GA, TC,GA] (default: TC)
            -b|--3base                - use HISAT-3N or Bismark aligner (default: off)
            -t|--stl                  - experiment is STL (default: off)
            -u|--mutpos               - call mutations by absolute genomic location (default: off)
            -w|--bigwig               - Make additional .bigWig track files (default: off)
            -a|--addchr               - add chr prefix to chromosome names during alignment (default: off)
            -n|--spikein              - substring identifying spikeins with gene_name tag value in .gtf file (default: no spikeins)
            --fasta                   - path to genome fasta file
            --annotation              - path to gtf genome annotation file
            --genome_index            - path to aligner genome index (HISAT-3N, HISAT, Bowtie2, Bismark)
            -p|--parallel             - number of cpus used by pipeline or Slurm jobs [int] (default: 20)
            -e|--mem                  - memory used by Slurm jobs [intG](default: 119G)
            -l|--slurm                - use Slurm for job scheduling (default: off)
            -j|--jobname              - name for Slurm job
            -@|--email                - email address for Slurm notifications
            -h|--help|-?              - help message
```

## Output
* In the output directory, you will find the alignment statistics in `alignment_stats` file and the `<sample_name>_tl.bam` files that have all the aligned reads with gene level annotations embedded in GF, EF and XF tags.
* In the `tracks.dir` sub-directory you will find all the tracks (.tdf, .bigWig) separated by numbers of mutations and types of mutations.
* In the `<sample_name>.dir` sub-directories you will find HTSeq output files with read counts per one for the following features:  
	1. Gene level  
	2. Mature RNA level  
	3. Exon level (reads that have any overlap with an exon)

* `master-<date>.csv.gz` file has alignment and mutations information for each read (read pair) in all samples.
* `cB-<date>.csv.gz` file contains aggregated information about number of reads that have the same number of mutations, U content, etc.
	- XF - Mature feature: when read is entirely exonic or spliced
	- GF - Gene feature: when read is aligned to any part of feature (intronic or exonic)
	- rname - Chromosome name
	- nT - Number of uridines in read (additional columns are added for other types of mutations)
	- TC - Number of called T-to-C mutations in read (additional columns are added for other types of mutations)
	- sj - Logical: TRUE if read contains exon-exon spliced junction
	- ai - Logical: TRUE if read aligns to any intronic region
	- io - Logical: TRUE if read aligns to only intronic regions
	- ei - Logical: TRUE if read aligns to intronic and exonic regions
	- sample - Sample name
	- n - Number of reads which have the identical set of values described above

* In the `scripts.dir` sub-directory you will find copy of all scripts including `config.sh` file with a record of parameter values used for the analysis.

## Optional Outputs
* If `--mutpos` or `mut_pos='TRUE'` is set the following additional outputs will be created.
  * In the `tracks.dir` you will find additional track files for each requested mutation type that contain the number of mutations at each individual nucleotide positions.
  * `cU-<data>.csv.gz` contains aggregated data for individual nucleotides. Note that positions where a mutation is never observed in any sample are not included.
        - gloc - The genomic position of the observed base
        - rname - Chromosome name
        - XF - Mature feature in which the base was observed (read must be entirely exonic or spliced)
        - GF - Gene feature in which the base was observed (read is aligned to any part of feature's intron or exon)
        - ai - Logical. TRUE if read containing the base aligns to any intronic region.
        - tp - The type of mutation observed
        - sample - Sample name
        - n - Number of mutations observed at this position
        - trials - Number of reads aligned to base at this position


* If `STL='TRUE'` the following additional outputs will be created.
  * In the `tracks.dir` you will find additional tracks for the single-base-pair position of the 5' and 3' position of each read separated by numbers of mutations, types of mutations, and if the tracks represent the 5' or 3' positions.
  * A new directory called `TSScall.dir` will be created within the master directory containing important files for STL-seq.
    * `cB_TSS.csv.gz` contains similar data as `cB-<date>.csv.gz`, but reads are grouped by `TSS_name` instead of gene information.
        - TSS_name - Arbitrary name for TSS called by TSScall (see TSScall manual for more information)
        - rname - Chromosome name
        - nT - Number of uridines in read (additional columns are added for other types of mutations)
        - TC - Number of called T-to-C mutations in read (additional columns are added for other types of mutations)
        - sample - Sample name
        - strand - DNA strand of the TSS
        - pos5 - Most 5' position of the read (5' always in reference to + DNA strand)
        - pos3 - Most 3' position of the read (3' always in reference to + DNA strand)
        - n - Number of reads which have the identical set of values described above
      * `JOB_NAME_TSS.bed` a bed file for all TSSs called by TSScall (see TSScall manual for more information).
      * `JOB_NAME_TSS_detail.bed` a bed file containing detailed information about each TSS including associated genes, transcripts, TSS clusters and more (see TSScall manual for more information).

* If `STL='TRUE'` and `mut_pos='TRUE'` the following additional outputs will be created.
  * `master-<date>.rds` will contain the additional variable 'lloc' which identified the local position within the read that the mutation was observed. Note this variable assumes all reads align to the positive strand (i.e. lloc of 10 from a 30 bp long read indicates a mutation at position 21 relative to the 5' end of the original RNA transcript which is derived from the minus strand).
