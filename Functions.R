# Functions to load

mdParse <- function(md, nm){
    # This is a central function to figure out which bases are mutated
    # It records the reference base at the site of mutations
    md.gsub <- gsub("([\\^]*[ACGT]+)[0]*", " \\1 ", md)
    # split each operation using strsplit
    md.spl  <- strsplit(md.gsub, "[ ]+")[[1]]

    this    <- as.integer()
    this <- lapply(md.spl, function(y) {
                if (!is.na(as.numeric(y))) {
                    o <- rep("M", as.numeric(y))
                } else if( length(grep("\\^", y)) > 0) {
                    o <- rep("D", nchar(y) - 1)
                } else if (nchar(y) == 1) {
                    o <- rep(y, 1)
                }
            }
    )
  this <- do.call(c, this)
  # # after this step, we have a vector of length =
  # # read length and each position telling if its a
  # # match(M), mismatch(with reference base entered) or deletion (D)
  return(this)
}

osParse <- function(CIGAR, SEQ){
    os <- 1
    right_os <- nchar(SEQ)
    if (!is.na(length(grep("S\\d", CIGAR)))) {
        if (length(grep("S\\d", CIGAR)) > 0) {
            CIGAR.gsub <- gsub("(\\d+)S", "\\1 ", CIGAR) # Add space
            CIGAR.spl <- strsplit(CIGAR.gsub, "[ ]+")[[1]]
            if (!is.na(as.numeric(CIGAR.spl[1]))){
                os <- os + as.numeric(CIGAR.spl[1])
            }
        }
    }

    if (!is.na(length(grep("S$", CIGAR)))) {
        if (length(grep("S$", CIGAR)) > 0) {
            CIGAR.gsub <- gsub("([\\^]*[SMNID]+)[0]*", " \\1 ", CIGAR) # Add space
            CIGAR.spl <- strsplit(CIGAR.gsub, "[ ]+")[[1]]
            if (CIGAR.spl[length(CIGAR.spl)] == "S"){
                if(!is.na(as.numeric(CIGAR.spl[(length(CIGAR.spl) - 1)]))) {
                    right_os <- right_os - as.numeric(unlist((CIGAR.spl[(length(CIGAR.spl) - 1)])))
                }
            }
        }
    }
    return(c(os, right_os))
}

trimString <- function(STRING, OS){
    if (length(OS) == 2){
        out <- str_sub(STRING, OS[1], OS[2])
    } else {
        out <- STRING
    }
}

whichMut <- function(MDP){
    out <- which(MDP %in% c('A', 'T', 'C', 'G'))
    return(out)
}

readFvR <- function(flag){
    out <- 'F'
    if (intToBits(flag)[1] == 1) { # Is it a paired end read?
        if (intToBits(flag)[5] == intToBits(flag)[7]){ # Will call any flag that is first of pair and RC, or second in pair and not RC.
            out <- 'R'
        }
    } else {
        if (intToBits(flag)[5] == 1) { # Will fix RC alignmnet
            out <- 'R'
        }
    }
    return(out)
}

mutType <- function(lloc, trimseq, mdparse, FR){
    q <- c()
    r <- c()
    if (length(mdparse) > 0){
        q <- strsplit(trimseq, '')[[1]][lloc]
        r <- mdparse[lloc]
    }
    if (FR == 'R'){
        q <- rcv[q]
        r <- rcv[r]
    }
    out <- (paste0(r, q))
    return(out)
}

gLocations <- function(CIGAR, POS, SEQ){
    gloc <- 1:nchar(SEQ) + POS - 1
    cp <- cigarRangesAlongReferenceSpace(CIGAR, with.ops = T)
    cpn <- cp@unlistData@NAMES
    l <- 1
    for (i in seq_along(cpn)){
        if (cpn[i] == 'S') {
        } else if (cpn[i] == 'M'){
            l <- l + cp@unlistData@width[i]
        } else if (cpn[i] == 'N'){
            gloc[l:length(gloc)] <- gloc[l:length(gloc)] + cp@unlistData@width[i]
        }
    }
    return(gloc)
}

getRefSeq <- function(trimseq, mdparse, lloc, FR){
    # function to get the RNA reference sequence.
    # Reverse complement if it is an 'R' read.

    s <- strsplit(trimseq, '')[[1]] # Make it into a vector
    s[lloc] <- mdparse[lloc] # Adjust all bases that disagree in MD tag

    if (FR == 'R'){ # Make
        s <- sapply(s, function(x) rcv[x])
    }
    return(s)
}

getMutQual <- function(lloc, qualasc){
    out <- qualasc[lloc]
    return(out)
}

mergeReads1 <- function(refseq, qualasc){

    # Initialize output data frame
    odf <- tibble(.rows = 1)

    # Count the number of each type of base in the reference, including only those that survive trimming and quality thresholding:
    odf$nA <- sum((unlist(refseq) == 'A' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nT <- sum((unlist(refseq) == 'T' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nG <- sum((unlist(refseq) == 'G' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nC <- sum((unlist(refseq) == 'C' & unlist(qualasc) > minqual), na.rm = TRUE)

    # Return the data_frame
    return(odf)
}

mergeReads2 <- function(gloc, refseq, qualasc, gmutloc, lloc, mutqual, tp, mdist){

    # Initialize output data frame
    odf <- tibble(.rows = 1)

    # Which bases overlap, and of those, which copy should be used (the one with the higher quality)?
    ab <- match(gloc[[1]], gloc[[2]])
    ia <- is.na(ab) | (qualasc[[1]] >= qualasc[[2]][ab])
    ib <- !(gloc[[2]] %in% gloc[[1]][ia])

    # Given these overlaps, assemble merged sequences for counting bases.
    odf$gloc <- c(gloc[[1]][ia], gloc[[2]][ib]) %>% list()
    refseq <- c(refseq[[1]][ia], refseq[[2]][ib])
    qualasc <- c(qualasc[[1]][ia], qualasc[[2]][ib])

    # Which mutations overlap, and of those which should we use for analysis (the one with the higher quality)?
    mab <- match(gmutloc[[1]], gmutloc[[2]])
    mia <- is.na(mab) | mutqual[[1]] >= mutqual[[2]][mab]
    mib <- !(gmutloc[[2]] %in% gmutloc[[1]][mia])

    # Merge locations (both globally and locally) of the mutations and save their top quality score.
    odf$gmutloc <- c(gmutloc[[1]][mia], gmutloc[[2]][mib]) %>% list()
    odf$lloc <- c(lloc[[1]][mia], lloc[[2]][mib]) %>% list()
    odf$mutqual <- c(mutqual[[1]][mia], mutqual[[2]][mib]) %>% list()
    odf$tp <- c(tp[[1]][mia], tp[[2]][mib]) %>% list()
    odf$mdist <- c(mdist[[1]][mia], mdist[[2]][mib]) %>% list()

    # Count the number of each type of base in the reference, including only those that survive trimming and quality thresholding:
    odf$nA <- sum((unlist(refseq) == 'A' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nT <- sum((unlist(refseq) == 'T' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nG <- sum((unlist(refseq) == 'G' & unlist(qualasc) > minqual), na.rm = TRUE)
    odf$nC <- sum((unlist(refseq) == 'C' & unlist(qualasc) > minqual), na.rm = TRUE)

    # Return the data_frame
    return(odf)
}

asc <- function(x) { strtoi(charToRaw(x), 16L) }

getGlobMutLoc <- function(lloc, gloc){
    out <- gloc[lloc]
    return(out)
}


# Not technically functions, but for RC calls:
rcv <- c('A', 'C', 'G', 'T', 'N') # Make a vector of the reverse complements of each base.
names(rcv) <- c('T', 'G', 'C', 'A', 'N') # Name it so we know which goes with which.

# Vector of possible mutations:
muttps <- c("AC","AG","AN","AT","CA","CG","CN","CT","GA","GC","GN","GT","TA","TC","TG","TN")

# Functions for counting script:
snpTest <- function(gmutloc, rname){
    return(as.character(gmutloc) %chin% snp_chr[[as.character(rname)]])
}

keepMut <- function(snprisk, mutqual, mdist){
  return(!snprisk & (mutqual > minqual) & (mdist > mindist))
}

countMutations <- function(tp, keep){
    out <- tp[[1]][keep[[1]]] %>%
        factor(levels = muttps) %>%
        table() %>%
        rbind() %>%
        data.frame()
    return(out)
}


