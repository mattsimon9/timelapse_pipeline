#!/bin/bash

# Source the paths and variables:
    source $1

if [ $step_scripts = 'TRUE' ]; then

    # Setup folders:

    mkdir -p $MASTER_DIR
    cd $MASTER_DIR

    if [ -d scripts.dir ]; then
        echo '* Moving old scripts directory'
        mv scripts.dir scripts.dir.old
    fi

    if [ -d tracks.dir ]; then
        echo '* Moving old tracks directory'
        mv tracks.dir tracks.dir.old
    fi

    mkdir tracks.dir
    mkdir scripts.dir

    if [ "$STL" = "TRUE" ]; then
        if [ -d TSScall.dir ]; then
            echo '* Moving old TSScall directory'
            mv TSScall.dir TSScall.dir.old
        fi
        mkdir TSScall.dir
    fi
        
    mkdir -p `echo ${samples[@]/%/.dir}`


    #   Make a copy of TimeLapse pipeline scripts
    cp `ls ${GIT_PATH}/* | grep -v config` ${MASTER_DIR}/scripts.dir
    cp $1 ${MASTER_DIR}/scripts.dir/config.sh

fi

echo "** Setup finished"
