#!/bin/bash

    source $1

# load tools
    if [[ -n $BCFTOOLS_MOD ]]; then
        module purge
        module load ${BCFTOOLS_MOD}
    fi
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi
    if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi
    if [[ -n $R_MOD ]]; then module load ${R_MOD}; fi

    cd $MASTER_DIR

if [ "$step_vcf" = "TRUE" ]; then
    # Loop through control samples:
        for cs in ${control_samples[@]}; do
            $SAMTOOLS sort -@ "$cpus" -o "$cs"_sort.bam "$cs"_tl.bam
            $SAMTOOLS index -@ "$cpus" "$cs"_sort.bam
        done

    # Parallelize SNPs calling. Each chromosome in each .bam file is processed as separate job
        # Note: This approach does not give the same snp.txt result. In 2199712 SNPs there were 16 different. 
        # {1} : path to fasta file 
        # {2} : samtools view -H ${control_samples[0]}_sort.bam | awk ' $1 == "@SQ" {split($2,a,":"); print a[2]}' : Extracts chromosome names from .bam file header
        # {3} : ${control_samples[@]/%/_sort.bam}                                                    : Appends "_sort.bam" to the end of control names and prints
        $GNUPARALLEL -j "$cpus" "$SAMTOOLS mpileup -uf {1} \
                                                   -r {2} {3} \
                                | $BCFTOOLS call -mv" ::: $genome_fasta \
                                                      ::: $($SAMTOOLS view -H ${control_samples[0]}_sort.bam \
                                                                    | awk ' $1 == "@SQ" {split($2,a,":"); print a[2]}') \
                                                      ::: ${control_samples[@]/%/_sort.bam} > snp.vcf


        # Note: Easier and also fast option would be:  bcftools mpileup --threads $cpus -f $genome_fasta "$cs"_sort.bam | bcftools call --threads $cpus-mv > snp-"$cs".vcf


    # Clean this up for all possible mutations:
        # Note: Filtering now inludes cases where both alleles are mutated
        awk '$1 !~ /^#/ && length($4) == 1 {if (length($5) == 1) {print $4":"$5":"$1":"$2} 
                                            else if (length($5) == 3 && $5 ~ /,/) {split($5, mut, ",")
                                                                                   print $4":"mut[1]":"$1":"$2
                                                                                   print $4":"mut[2]":"$1":"$2} 
                                            }' snp.vcf \
            | sort \
            | uniq > snp.txt

        echo '* SNPs called and snp.txt generated'
fi


if [ "$step_norm" = "TRUE" ]; then
    # Get normalization values for tracks
        $NORMALIZE_R \
            $( if [ ! -z $spikein_name ]; then echo "-s $spikein_name"; fi ) \
            -e $R_debug > normalize.Rout &&

        if [ -e scale ]; then
                echo '* Scale file successfully made'
        else
                echo '! WARNING: no scale file made'
        fi
fi