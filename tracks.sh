#!/bin/bash
#
# To make filtered bam files and tracks with different numbers of mutations

# Get parameters
# This allows to be script run as: sbatch tracks.sh configFile sampleName
    if [[ -n $1 ]]; then config=$1; fi
    if [[ -n $2 ]]; then sample=$2; fi

    source $config

# Set normalization value
    if [ -e "$MASTER_DIR"/scale ]; then
            normVal=$(awk -v sam=$sample '$1 == sam {print $2}' "$MASTER_DIR"/scale)
    else
            normVal='1'
            echo '* No scale file provided...setting normVal to one.'
    fi

# Test if count files exist
    readFilesCount=$(ls "$MASTER_DIR"/"$sample"_*_reads.txt 2> /dev/null | wc -l)
    expectedCount=$((6 * $(echo $mut_tracks | sed 's/,/ /g' | wc -w) ))

    if [ $readFilesCount -eq $expectedCount ]; then
            echo '* Read file found for each level of counting.'
    else
            echo '* Creating files for each level of counting.'

            if [[ -n $PYTHON_MOD ]]; then module load ${PYTHON_MOD}; fi
            if [[ -n $PYSAM_MOD ]]; then module load ${PYSAM_MOD}; fi

            cd $MASTER_DIR
            $PYTHON $TLcount2tracks \
                -i "$sample"_counts.csv.gz \
                -s $sample \
                --mutType $mut_tracks

    fi

# Load tools
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi
    if [[ -n $IGVTOOLS_MOD ]]; then module load ${IGVTOOLS_MOD}; fi
    if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi
    if [[ -n $BDG2BIGWIG_MOD ]]; then module load ${BDG2BIGWIG_MOD}; fi

    if [ "$track_prog" = 'STAR' ]; then
        if [[ -n $STAR_MOD ]]; then module load ${STAR_MOD}; fi
    fi
    
    if [ "$STL" = "TRUE" ] || [ "$mut_pos" = "TRUE" ] || [ ! $track_prog = 'STAR' ]; then 
        if [[ -n $BEDTOOLS_MOD ]]; then module load ${BEDTOOLS_MOD}; fi
    fi


    cd $MASTER_DIR/"$sample".dir


# Sort the starting .bam file by coordinate
    $SAMTOOLS sort -@ "$cpus" -o "$sample"_sort.bam ../"$sample"_tl.bam
    $SAMTOOLS index -@ "$cpus" "$sample"_sort.bam

# Make .chrom.sizes file from .bam file header (alternative to .genome file for toTDF)
    chrom_sizes=$(echo ${genome_fasta##*/} | cut -f 1 -d '.')".chrom.sizes"
    if [ ! -f $chrom_sizes ]; then
                samtools view -H "$sample"_sort.bam \
                    | awk -v OFS="\t" ' $1 ~ /^@SQ/ {split($2, chr, ":")
                                                     split($3, size, ":")
                                                     print chr[2], size[2]}' > "$chrom_sizes"
    fi


    muts=$(echo $mut_tracks | tr ',' ' ')

    for b in $muts; do
        if [ $b == "GA" ]; then
            colVal[0]='200,200,200'
            colVal[1]='120,188,230'
            colVal[2]='65,125,195'
            colVal[3]='36,110,182'
            colVal[4]='27,78,165'
            colVal[5]='18,50,120'
        else
            colVal[0]='200,200,200'
            colVal[1]='250,150,150'
            colVal[2]='250,0,0'
            colVal[3]='150,0,0'
            colVal[4]='100,0,0'
            colVal[5]='50,0,0'
        fi

        # Make track headers
        for count in $(seq 0 5); do

            echo "track type=bedGraph name=\" $sample $b $count "plus \" description=\" $sample $b $count "positive strand\" visibility=full autoScale=off windowingFunction=mean viewLimits=0:10 color="${colVal[$count]} " altColor=10,10,10 priority=20" > ../tracks.dir/"$sample"."$b"."$count".pos.bedGraph
            echo "track type=bedGraph name=\" $sample $b $count "minus \" description=\" $sample $b $count "minus strand\" visibility=full negateValues=on autoScale=off windowingFunction=mean viewLimits=-10:0 color=10,10,10 altColor="${colVal[$count]} " priority=20" > ../tracks.dir/"$sample"."$b"."$count".min.bedGraph
            
            if [ "$STL" = "TRUE" ]; then
                echo "track type=bedGraph name=\" 5' $sample $b $count "plus \" description=\" $sample $b $count "positive strand\" visibility=full autoScale=off windowingFunction=mean viewLimits=0:10 color="${colVal[$count]} " altColor=10,10,10 priority=20" > ../tracks.dir/"$sample"."$b"."$count".5.pos.bedGraph
                echo "track type=bedGraph name=\" 5' $sample $b $count "minus \" description=\" $sample $b $count "minus strand\" visibility=full negateValues=on autoScale=off windowingFunction=mean viewLimits=-10:0 color=10,10,10 altColor="${colVal[$count]} " priority=20" > ../tracks.dir/"$sample"."$b"."$count".5.min.bedGraph

                echo "track type=bedGraph name=\" 3' $sample $b $count "plus \" description=\" $sample $b $count "positive strand\" visibility=full autoScale=off windowingFunction=mean viewLimits=0:10 color="${colVal[$count]} " altColor=10,10,10 priority=20" > ../tracks.dir/"$sample"."$b"."$count".3.pos.bedGraph
                echo "track type=bedGraph name=\" 3' $sample $b $count "minus \" description=\" $sample $b $count "minus strand\" visibility=full negateValues=on autoScale=off windowingFunction=mean viewLimits=-10:0 color=10,10,10 altColor="${colVal[$count]} " priority=20" > ../tracks.dir/"$sample"."$b"."$count".3.min.bedGraph
            fi

            if [ "$mut_pos" = "TRUE" ]; then
                echo "track type=bedGraph name=\" Mut position $sample $b "plus \" description=\" $sample $b "positive strand\" visibility=full autoScale=off windowingFunction=maximum viewLimits=0:10 color="${colVal[2]} " altColor=10,10,10 priority=20" > ../tracks.dir/"$sample"."$b".muts.pos.bedGraph
                echo "track type=bedGraph name=\" Mut position $sample $b "minus \" description=\" $sample $b "minus strand\" visibility=full negateValues=on autoScale=off windowingFunction=maximum viewLimits=-10:0 color=10,10,10 altColor="${colVal[2]} " priority=20" > ../tracks.dir/"$sample"."$b".muts.min.bedGraph
            fi


        done
    done


    # Filter the reads 
    $GNUPARALLEL -j 1 $SAMTOOLS view -@ "$cpus" \
                                     -b \
                                     -N ../"$sample"_{1}_{2}_reads.txt \
                                     -o ../tracks.dir/"$sample"_{1}_{2}.bam \
                                     "$sample"_sort.bam ::: $muts \
                                                        ::: $(seq 0 5)
                                       
    # Choose star or bedtools:
    if [ "$track_prog" = 'STAR' ]; then

        # Make tracks
        $GNUPARALLEL -j "$cpus" $STAR \
                                --runMode inputAlignmentsFromBAM \
                                --inputBAMfile ../tracks.dir/"$sample"_{1}_{2}.bam \
                                --outWigType bedGraph \
                                --outWigNorm None \
                                --outWigStrand Stranded \
                                --outFileNamePrefix ./{1}_{2}_ ::: $muts \
                                                               ::: $(seq 0 5)



        # Take only unique component of track
        $GNUPARALLEL -j "$cpus" "awk -v norm=${normVal} \
                                        '{print \$1, \$2, \$3, {3}1*norm*\$4}' \
                                        {1}_{2}_Signal.Unique.{4}.out.bg \
                                        >> ../tracks.dir/${sample}.{1}.{2}.{5}.bedGraph" ::: $muts \
                                                                                         ::: $(seq 0 5) \
                                                                                         ::: + - \
                                                                                         :::+ str1 str2 \
                                                                                         :::+ pos min

        rm *.bg

    else

        $GNUPARALLEL -j "$cpus" "$BEDTOOLS genomecov \
                                        -ibam ../tracks.dir/${sample}_{1}_{2}.bam \
                                        -bg \
                                        -pc \
                                        -strand {3} \
                                    | awk -v norm=${normVal} \
                                        '{print \$1, \$2, \$3, {3}1*norm*\$4}' \
                                        >> ../tracks.dir/${sample}.{1}.{2}.{4}.bedGraph" ::: $muts \
                                                                                         ::: $(seq 0 5) \
                                                                                         ::: + - \
                                                                                         :::+ pos min
    fi


    # Make tdf files from the tracks
    $GNUPARALLEL -j "$cpus" $IGVTOOLS toTDF \
                                -f mean,max \
                                ../tracks.dir/"$sample".{1}.{2}.{3}.bedGraph \
                                ../tracks.dir/"$sample".{1}.{2}.{3}.tdf \
                                "$chrom_sizes" ::: $muts \
                                             ::: $(seq 0 5) \
                                             ::: pos min




    if [ "$STL" = "TRUE" ]; then
        

        # Make 5' and 3' coverage tracks
        $GNUPARALLEL -j "$cpus" "$BEDTOOLS genomecov \
                                        -ibam ../tracks.dir/${sample}_{1}_{2}.bam \
                                        -{3} \
                                        -dz \
                                        -strand {4} \
                                    | awk -v norm=${normVal} \
                                        '{print \$1, \$2, \$2+1 , {4}1*norm*\$3}' \
                                        >> ../tracks.dir/${sample}.{1}.{2}.{3}.{5}.bedGraph" ::: $muts \
                                                                                             ::: $(seq 0 5) \
                                                                                             ::: 5 3 \
                                                                                             ::: + - \
                                                                                             :::+ pos min

        # Make tdf files from the tracks
        $GNUPARALLEL -j "$cpus" $IGVTOOLS toTDF \
                                    -f mean,max \
                                    ../tracks.dir/"$sample".{1}.{2}.{3}.{4}.bedGraph \
                                    ../tracks.dir/"$sample".{1}.{2}.{3}.{4}.tdf "$chrom_sizes" ::: $muts \
                                                                                               ::: $(seq 0 5) \
                                                                                               ::: 5 3 \
                                                                                               ::: pos min
    fi

    if [ "$mut_pos" = "TRUE" ]; then

        # Scale mutation bed file
        $GNUPARALLEL -j "$cpus" "awk -v OFS='\t' '{print \$1, \$2, \$3, {2}1*norm*\$4}' \
                                                    norm=${normVal} \
                                                    ../tracks.dir/${sample}_{1}_{3}_muts.bedGraph\
                                | bedtools sort -chrThenSizeA \
                                    >> ../tracks.dir/${sample}.{1}.muts.{3}.bedGraph" ::: $muts \
                                                                                      ::: + - \
                                                                                      :::+ pos min

        $GNUPARALLEL -j "$cpus" $IGVTOOLS toTDF \
                                    -f mean,max \
                                    ../tracks.dir/"$sample".{1}.muts.{2}.bedGraph \
                                    ../tracks.dir/"$sample".{1}.muts.{2}.tdf "$chrom_sizes" ::: $muts \
                                                                                            ::: pos min
    fi
    
    echo "* TDF track files created for sample $sample."

    if [ "$bigwig" = "TRUE" ]; then
        $GNUPARALLEL -j "$cpus" $BDG2BIGWIG {1} "$chrom_sizes" {1.}.bigWig ::: $(ls ../tracks.dir/"$sample"*.bedGraph)

        echo "* bigWig track files created for sample $sample."
    fi

    if [ "$STL" = "TRUE" ]; then
        mv "$sample"_sort.bam ../TSScall.dir/.
        mv "$sample"_sort.bam.bai ../TSScall.dir/.
    fi

    if [ "$cleanup" = "TRUE" ]; then
        rm -f *.bam
        rm -f ../"$sample"_*_reads.txt
        rm -f ../tracks.dir/"$sample"_*.bam
        rm -f ../tracks.dir/"$sample"_*.bedGraph
    fi
