#!/bin/bash

# Source the paths and variables:
    source $1
    sample=$2

# load tools
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi


    cd "$MASTER_DIR"/"$sample".dir

# fixmates:
    $SAMTOOLS fixmate -@ "$cpus" "$sample"Aligned.out.bam "$sample"_fixed_mate.bam &&

    echo "* Read mates fixed for sample $sample"


# Make filtered sam file:
    if [ "$FORMAT" = "NU" ]; then
        $SAMTOOLS view -@ "$cpus" -h "$sample"_fixed_mate.bam \
            | awk '$1 ~ /^@/ {print}
                   (($2 == 147 || $2 == 99) || ($2 == 83 || $2 == 163)) || (($2 == 355 || $2 == 403) || ($2 == 339 || $2 == 419))  {print}' > "$sample".f.sam
    elif [ "$FORMAT" = "PE" ]; then
        $SAMTOOLS view -@ "$cpus" -q 2 -h "$sample"_fixed_mate.bam \
            | awk '$1 ~ /^@/ {print}
                  ($2 == 147 || $2 == 99) || ($2 == 83 || $2 == 163) {print}' > "$sample".f.sam
    elif [ "$FORMAT" = "SE" ]; then
        $SAMTOOLS view -@ "$cpus" -q 2 -h "$sample"_fixed_mate.bam \
            | awk '$1 ~ /^@/ {print}
                  ($2 == 0 || $2 == 16) {print}' > "$sample".f.sam
    else
        $SAMTOOLS view -@ "$cpus" -h "$sample"_fixed_mate.bam > "$sample".f.sam
    fi &&
    echo "* Reads filtered for sample $sample"


# Sort the sam file
    $SAMTOOLS sort -@ "$cpus" -n -o "$sample".s.sam "$sample".f.sam &&

    echo "* Filtered reads sorted for sample $sample"

# Clean up
    if [ "$cleanup" = "TRUE" ]; then
        rm "$sample".f.sam
        rm "$sample"_fixed_mate.bam
    fi
