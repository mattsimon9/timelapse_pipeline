#!/bin/bash

# Source the paths and variables:
    cd "$MASTER_DIR"
    source $1
    sample=$2

    cd "$MASTER_DIR"/"$sample".dir


# align trimmed reads to the  genome
    if [[ -n $BOWTIE2_MOD ]]; then module load ${BOWTIE2_MOD}; fi
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi

    echo "* Aligning reads with Bowtie2 for sample $sample"


    if [[ "$READS" == "F" ]]; then
        $BOWTIE2 \
            -p $cpus \
            -x $BOWTIE_INDEX \
            --very-sensitive-local \
            -N 1 \
            -U "$sample".t.fastq \
            --mp 4,2 \
            -S "$sample".sam
    elif [[ "$READS" == "FR" ]]; then
        $BOWTIE2 \
            -p $cpus \
            -x $BOWTIE_INDEX \
            --very-sensitive-local \
            -N 1 \
            -1 "$sample".t.r1.fastq \
            -2 "$sample".t.r2.fastq \
            --mp 4,2 \
            -S "$sample".sam
    elif [[ "$READS" == "RF" ]]; then
        $BOWTIE2 \
            -p $cpus \
            -x $BOWTIE_INDEX \
            --very-sensitive-local \
            -N 1 \
            -1 "$sample".t.r2.fastq \
            -2 "$sample".t.r1.fastq \
            --mp 4,2 \
            -S "$sample".sam
    else
        echo "! No FR/RF/F method recognized for " $sample
    fi


    if [[ "$FORMAT" = "SE" ]]; then
        rm "$sample".t.fastq
    else
        rm "$sample".t.r[12].fastq
    fi


    $SAMTOOLS view -@ "$cpus" -o "$sample"Aligned.out.bam "$sample".sam

    rm "$sample".sam

    echo "* Alignment script finished for " $sample
