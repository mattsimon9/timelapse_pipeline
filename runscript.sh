#!/bin/bash
# TimeLapse-seq run script .
# Developed originally for Schofield & Simon 2017
# ver March 2019 to work as a fully automated script with SLURM
# Usage:
#        This script is intended to be initiated by executing run.sh wrapper script to work properly
#

# function that retrieves IDs of the last submitted jobs and waits until they are finished.
# Input is number of jobs that were submitted (waitForJobs 3 -or- waitForJobs ${#samples[@]})
# Second paramenter determines whether to terminate pipeline if a job fails (waitForJobs 3 quit)
function waitForJobs() {

    # Get JOB_IDs (take IDs of the last x submitted jobs)
        local myjob=$(squeue -hu $USER -O "submittime,jobid" | sort -r | head -n $1 | cut -d " " -f 2)
        local code=0

    # Check if jobs are still running
        while [ $(squeue -hj $(echo $myjob | sed 's/ /,/g') | wc -l ) != 0 ]; do
            sleep 1m

            # Check if any of the jobs failed
            if [[ $(sacct -bpn -j $(echo $myjob | sed 's/ /,/g') | grep -v "\." | cut -d "|" -f 2) =~ "FAILED" ]]; then
                if [ "$code" = 0 ]; then
                    echo "!!! One or more jobs failed"
                    local code=1
                fi

                if [ "$2" = "quit" ]; then
                    echo "!!! Terminating all remaning jobs"
                    scancel $myjob
                    echo "!!! Terminating pipeline"
                    exit 1
                fi

            fi

        done

        return $code
}

# Read settings
    source $1

### Set up destination directories:
    if [ "$step_scripts" = "TRUE" ]; then
        $SETUP_SCRIPT $1
    fi

### Submit preprocessing jobs to get to sample_htseq.bam files:
    cd $MASTER_DIR
    if  [ "$step_copyfq" = "TRUE" ] || \
        [ "$step_fastuniq" = "TRUE" ] || \
        [ "$step_cutadapt" = "TRUE" ] || \
        [ "$step_align" = "TRUE" ] || \
        [ "$step_readfilter" = "TRUE" ] || \
        [ "$step_feature_count" = "TRUE" ] || \
        [ "$step_fragment" = "TRUE" ]; then

        if [ "$SLURM" = "TRUE" ]; then
            for sample in ${samples[@]}; do
                sbatch \
                    --job-name=pre_"$sample" \
                    --output="$sample"_pre.txt \
                    --ntasks=1 \
                    --cpus-per-task="$cpus" \
                    --time=48:00:00 \
                    --mem="$mem" \
                    --mail-type=END \
                    --mail-user="$email" \
                    --export=sample=$sample,config=$1 \
                    $TLpreprocess

                echo '* Submitted preprocessing job for sample:' $sample
            done

            # Wait for jobs to finish
                waitForJobs ${#samples[@]} quit &&
                echo '** All preprocessing jobs finished'

            # Gather alignment stats:
                grep -A 15 'reads; of these:' *_pre.txt > alignment_stats.txt

        else
            for sample in ${samples[@]}; do
                echo '* Preprocessing sample:' $sample
                $TLpreprocess $1 $sample
            done &&
            echo '** All preprocessing jobs finished' ||
            (echo '!! Preprocessing of sample(s) failed'; exit 1)
        fi

    fi

    if [ "$cleanup" = "TRUE" ]; then
        rm -f *.dir/*fixed_mate.bam

        echo '* Cleaning up fixed mate .bam files'
    fi

### Get normalization values for tracks:
    cd $MASTER_DIR
    if [ "$step_norm" = "TRUE" ] || \
       [ "$step_vcf" = "TRUE" ]; then

        echo '* Running SNP normalization'

        if [ "$SLURM" = "TRUE" ]; then
            sbatch \
                --job-name=SNPnorm \
                --output=SNPnorm.txt \
                --nodes=1 \
                --cpus-per-task="$cpus" \
                --time=48:00:00 \
                --mem="$mem" \
                --mail-type=END \
                --mail-user="$email" \
                $RUN_SNP_NORM $1


            waitForJobs 1 quit &&
            echo '** SNP calling and normalization finished'

        else
            $RUN_SNP_NORM $1 &&
            echo '** SNP calling and normalization finished' ||
            (echo '!! SNP calling or normalization failed'; exit 1)
        fi
    fi

### Start the mutation calling:
    if [ "$step_muts" = "TRUE" ]; then

        if [ "$SLURM" = "TRUE" ]; then
            for sample in ${samples[@]}; do
                sbatch \
                    --job-name=mut_"$sample" \
                    --output="$sample"_muts.txt \
                    --nodes=1 \
                    --cpus-per-task="$cpus" \
                    --time=48:00:00 \
                    --mem="$mem" \
                    --mail-type=END \
                    --mail-user="$email" \
                    --export=sample=$sample,config=$1 \
                    $TLmuts

                echo '* Submitted mutation calling job for sample:' $sample
            done

            # Wait for jobs to finish
            waitForJobs ${#samples[@]} quit &&
            echo '** All mutation calling jobs finished'

        else
            for sample in ${samples[@]}; do
                echo '* Calling mutations for sample:' $sample
                $TLmuts $1 $sample
            done &&
            echo '** All mutation calling jobs finished' ||
            (echo '!! Preprocessing of sample(s) failed'; exit 1)
        fi


    fi

### Start tracks building:
    if [ "$step_tracks" = "TRUE" ]; then

        if [ ! -e "$MASTER_DIR"/scale ]; then
            echo '!! Warning: Failed creating scale file or no scale file provided. All tracks will be scaled with factor 1'
        fi


        if [ $SLURM = "TRUE" ]; then
            for sample in ${samples[@]}; do
                sbatch \
                    --job-name=trk_"$sample" \
                    --output="$sample"_tracks.txt \
                    --nodes=1 \
                    --cpus-per-task="$cpus" \
                    --time=48:00:00 \
                    --mem="$mem" \
                    --mail-type=END \
                    --mail-user="$email" \
                    --export=sample=$sample,config=$1 \
                    $TLtracks

                echo '* Submitted tracks creating job for sample:' $sample
            done

            # Wait for jobs to finish
            waitForJobs ${#samples[@]} quit &&
            echo '** All tracks jobs finished'
        else
            for sample in ${samples[@]}; do
                echo '* Generating tracks for sample:' $sample
                $TLtracks $1 $sample
            done &&
            echo '** All tracks jobs finished' ||
            (echo '!! Creating tracks for sample(s) failed'; exit 1)
        fi



        $TLmakeIGVsession $1 &> /dev/null &&
        echo '** Created IGV session file'

    fi

### Make a combined master file of the data:
    if [ "$step_master" = "TRUE" ]; then
        cd $MASTER_DIR

        if [ "$SLURM" = "TRUE" ]; then
            sbatch \
                --job-name=master_build \
                --output=master_build.txt \
                --nodes=1 \
                --ntasks-per-node=1 \
                --time=48:00:00 \
                --mem="$mem" \
                --mail-type=END \
                --mail-user="$email" \
                $TLmaster $1

            # Wait for jobs to finish
            waitForJobs 1 &&
            echo '** Master file successfully created'
        else
            $TLmaster $1 &&
            echo '** Master file successfully created' ||
            (echo '!! Creating master file failed'; exit 1)
        fi
    fi

  ### If STL-seq, run TSScall.py and remake master file
    if [ "$STL" = "TRUE" ] && \
       [ "$step_master" = "TRUE" ]; then
        cd $MASTER_DIR

        if [ "$SLURM" = "TRUE" ]; then
            sbatch \
                --job-name=TSScall \
                --output=TSScall.txt \
                --nodes=1 \
                --cpus-per-task="$cpus" \
                --time=48:00:00 \
                --mem="$mem" \
                --mail-type=END \
                --mail-user="$email" \
                $TSScall $1

            # Wait for jobs to finish
            waitForJobs 1 &&
            echo '** STL-seq master file successfully created'
        else
            $TSScall $1 &&
            echo '** STL-seq master file successfully created' ||
            (echo '!! Creating STL-seq master file failed'; exit 1)
        fi
    fi


echo '*** TimeLapse pipeline finished'
printf 'Runtime: %02dh:%02dm:%02ds\n' $(($SECONDS/3600)) $(($SECONDS%3600/60)) $(($SECONDS%60))
