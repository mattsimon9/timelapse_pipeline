#!/bin/bash

# Source the paths and variables:
    source $1
    sample=$2

# load tools
    if [[ -n $HTSEQ_MOD ]]; then module load ${HTSEQ_MOD}; fi
    if [[ -n $PYTHON_MOD ]]; then module load ${PYTHON_MOD}; fi
    if [[ -n $SAMTOOLS_MOD ]]; then module load ${SAMTOOLS_MOD}; fi
    if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi


    cd $MASTER_DIR/"$sample".dir

    # Run three counting processes at the same time with modified htseq-count in parallel mode
        # GF : gene | gene_id | union               => all reads over annotated gene
        # EF : exon | gene_id | union               => reads that touch exons
        # XF : exon | gene_id | intersection-strict => mature only



    $SAMTOOLS view -h -@ "$cpus" "$sample".s.sam \
        | $GNUPARALLEL \
            -L 2 \
            -j "$cpus" \
            --linebuffer \
            --joblog htseq_parallel.log \
            --roundrobin \
            --header '(@.*\n)*' \
            --pipe $PYTHON $TLhtseq_count \
                        -f sam \
                        --samout "$sample"_htseq.{#}_temp.sam \
                        -t gene,exon,exon \
                        -i gene_name,gene_name,gene_name \
                        -m union,union,intersection-strict \
                        -c GF_htseq.{#}_temp.txt,EF_htseq.{#}_temp.txt,XF_htseq.{#}_temp.txt \
                        - \
                        $annotation_gtf

    # Error catching
    if [ $(cut -f 7 htseq_parallel.log | grep '1' | wc -l ) -eq 0 ]; then
        echo '* HTSeq counting finished for sample' $sample
    else
        echo '!!! HTSeq counting failed!'
        exit 1
    fi


# Combine outputs from individual jobs
    # .sam file
    $SAMTOOLS view -H "$sample".s.sam \
        | cat - "$sample"_htseq.*_temp.sam \
        | $SAMTOOLS sort \
            -@ "$cpus" \
            -n \
            -o "$sample"_tl.bam -

    rm "$sample"_htseq.*_temp.sam
    echo "* HTSeq .sam files merged for sample $sample"

    # gene counts
    $GNUPARALLEL -j "$cpus" "awk -v 'OFS=\t' '
                                          {
                                                gene[\$1] += \$2
                                          }
                                          END {
                                                for (name in gene) {
                                                    print name, gene[name]
                                                }
                                          }' {1}_htseq.*_temp.txt  \
                            | LC_COLLATE=C sort -k1,1 > {2}.${sample}_htseq.txt" ::: GF EF XF \
                                                                                 :::+ gene exon_w_overlaps mature

    rm *_htseq.*temp.txt
    mv "$sample"_tl.bam $MASTER_DIR/.

    echo "* HTSeq counts files merged for sample $sample"
