#!/bin/bash
# Main sript for mutation calling in samples

# This allows to be script run as: sbatch sample_muts.sh configFile sampleName
    if [[ -n $1 ]]; then config=$1; fi
    if [[ -n $2 ]]; then sample=$2; fi  

# Source the paths and variables:
    source $config

    if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi
    if [[ -n $PIGZ_MOD ]]; then module load ${PIGZ_MOD}; fi
    if [[ -n $PYSAM_MOD ]]; then module load ${PYSAM_MOD}; fi
    if [[ -n $PYTHON_MOD ]]; then module load ${PYTHON_MOD}; fi

    cd $MASTER_DIR


# Call mutations
    $GNUPARALLEL -j $cpus "$PYTHON $TLmut_call -b {1} \
                                              --mutType $mut_tracks \
                                              --minQual $minqual \
                                              $( if [ $step_tracks = 'TRUE' ]; then echo '--tracks '; fi ) \
                                              $( if [ $mut_pos = 'TRUE' ]; then echo '--mutPos '; fi ) \
                                              --reads $FORMAT" ::: *_"$sample"_frag.bam


    echo "** Mutations called for sample $sample"


# Combine output from fragments into single file
    # 1) _count.csv files
    awk 'FNR > 1 || NR == 1' *_"$sample"_frag_counts.csv \
        | $PIGZ -p $cpus > "$sample"_counts.csv.gz

    rm *_"$sample"_frag_counts.csv


    # 2) _reads.txt files
    if [ $step_tracks = 'TRUE' ]; then
        $GNUPARALLEL -j $cpus "cat *_${sample}_frag_{1}_{2}_reads.txt > ${sample}_{1}_{2}_reads.txt" ::: $(echo $mut_tracks | tr ',' ' ') \
                                                                                                     ::: $(seq 0 5)
        rm *_"$sample"_frag_*_reads.txt
    fi


    # 3) _muts.csv files
    if [ "$step_mutsRDS" = 'TRUE' ]; then
        awk -v FS="," ' FNR > 1 { 
                                    trial[$1","$2] += $3
                                    n[$1","$2] += $4 
                                }
                        END     { 
                                    print "rname,gloc,trials,n"
                                    for (pos in trial) {
                                        if (n[pos] > 0)
                                            print pos","trial[pos]","n[pos]
                                    }
                                } ' *_"$sample"_frag_muts.csv \
            | $PIGZ -p $cpus > "$sample"_muts.csv.gz

        rm *_"$sample"_frag_muts.csv
    fi

    # 4) .bedGraph files
    if [ $mut_pos = 'TRUE' ]; then
        $GNUPARALLEL -j $cpus "awk -v 'OFS=\t' '
                                            {
                                                bdg[\$1\":\"\$2] += \$4
                                            }
                                            END {
                                                for (pos in bdg) {
                                                    split(pos, p, \":\")
                                                    print p[1], p[2], p[2] + 1, bdg[pos]
                                                    }
                                            }' *_${sample}_frag_{1}_{2}_muts.bedGraph > tracks.dir/${sample}_{1}_{2}_muts.bedGraph" ::: $(echo $mut_tracks | tr ',' ' ') \
                                                                                                                                    ::: pos min

        rm  *_"$sample"_frag_*_muts.bedGraph
    fi    

    # 5) _cU.csv files
    if [ $mut_pos = 'TRUE' ]; then
        # Pre-sort cU fragments
        $GNUPARALLEL -j $cpus "tail -n +2 {1} \
                                | LC_COLLATE=C sort > {1.}_sort.csv" ::: *_"$sample"_frag_cU.csv
        rm *_"$sample"_frag_cU.csv                    

        # Combine pre-sorted fragments
        LC_COLLATE=C sort -m *_"$sample"_frag_cU_sort.csv > "$sample"_cU_comb.csv 
        rm *_"$sample"_frag_cU_sort.csv

        # Get ammount of data
        cUsize=$(wc -l "$sample"_cU_comb.csv | cut -d ' ' -f 1)

        # Split file to sorted fragments keeping the same position groups in the same file fragment 
        awk -v FS="," \
            -v cpus=$cpus \
            -v cUsize=$cUsize \
            -v sample=$sample \
            'BEGIN { 
                    fragment_size = int(cUsize / cpus) + 1
                    i = 1
            }
            NR == (fragment_size * i - 1 ) { x = $1","$2 } 

            NR < (fragment_size * i ) { print >> i"_"sample"_cU_comb.csv" }

            NR >= (fragment_size * i ) { if ($1","$2 == x) 
                                            { 
                                                print >> i"_"sample"_cU_comb.csv"
                                            } 
                                        else 
                                            {           
                                                i++
                                                print >> i"_"sample"_cU_comb.csv"
                                            }
                
            }' "$sample"_cU_comb.csv

        rm "$sample"_cU_comb.csv

        # Awk script for processing sorted cU files; assumes all same positions are grouped together in subsequent lines
            # Thanks to this we have to keep in memory only one genomic position at given time
        function awkProcessCU () {
            awk -v FS="," 'NR == 1 {
                        nuc = $1":"$2
                }
                $1":"$2 != nuc {
                        for (pos in trial) {
                            if (n[pos] > 0)
                                print pos","trial[pos]","n[pos]
                        }
                        delete trial
                        delete n
                        nuc = $1":"$2
                }
                $1":"$2 == nuc {
                        trial[$1","$2","$3","$4","$5","$6] += $7
                        n[$1","$2","$3","$4","$5","$6] += $8
                }
                END {
                        for (pos in trial) {
                            if (n[pos] > 0)
                                print pos","trial[pos]","n[pos]
                        }
                }' $1
        }

        export -f awkProcessCU

        # Add header and process sorted fragments in parallel
        cat <(echo "rname,gloc,GF,XF,ai,tp,trials,n") \
            <($GNUPARALLEL -j $cpus awkProcessCU {1} ::: *_${sample}_cU_comb.csv) \
            | $PIGZ -p $cpus > "$sample"_cU.csv.gz

        rm *_${sample}_cU_comb.csv
    fi


    echo "** Results fragments merged into final files: ${sample}_count.csv.gz, ${sample}_muts.csv.gz, ${sample}_*_reads.txt, ${sample}_*_(pos|min)_muts.bedGraph and "$sample"_cU.csv.gz"
    
# Cleanup
    if [ "$cleanup" = "TRUE" ]; then
        rm -f *_"$sample"_frag.bam

        echo '* Cleaning up fragmented .bam files'
    fi
