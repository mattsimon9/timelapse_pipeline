#!/bin/bash

# Source the paths and variables:
    source $1

# Set name of sample:
    sample=$2

# Define path to sample data and start in correct dir
    cd $MASTER_DIR
    cd "$sample".dir


# Copy .fastq or .fastq.gz files into dir and create individual r1:
    if [ "$step_copyfq" = "TRUE" ]; then
        # Check that files exist
        if [[ -f $(echo ${LINK_BASE}/${prefix}${sample}/*.fastq | cut -f 1 -d " ") ]]; then
            suffix="fastq"
        elif [[ -f $(echo ${LINK_BASE}/${prefix}${sample}/*.gz | cut -f 1 -d " ") ]]; then
            suffix="gz"
        else
            echo "!!! No fastq files found at location ${LINK_BASE}/${prefix}${sample}"
            exit 1
        fi


        # parallel copying of files
        if [[ -n $GNUPARALLEL_MOD ]]; then module load ${GNUPARALLEL_MOD}; fi

        parallel -j $cpus "cp {1} ./" :::  ${LINK_BASE}/${prefix}${sample}/*.${suffix}

        echo "* .${suffix} files copied for sample $sample"


        # Parallel decompression
        if [[ $suffix == "gz" ]]; then
            if [[ -n $PIGZ_MOD ]]; then module load ${PIGZ_MOD}; fi

            $PIGZ -d -p "$cpus" *.gz &&
            echo "* .fastq files decompressed for sample $sample"
        fi

        cat *R1* > "$sample"_1.fastq

        rm -f *R1*

    else
        echo "* Skipping copy fastq step"
    fi


    echo "* Skipping fastuniq step for SE data"
    in="$sample"_1.fastq


    if [ "$step_cutadapt" = "TRUE" ]; then
        echo "* Running cutadapt in parallel mode for sample $sample"

        if [[ -n $CUTADAPT_MOD ]]; then module load ${CUTADAPT_MOD}; fi

        if [ "$STL" = "TRUE" ]; then
            $CUTADAPT \
                -a ^CGATC...GATCGGAAGAGC \
                -a ^CGATC...GGAAGAGCACAC \
                -n 2 \
                --minimum-length=20 \
                --cores="$cpus" \
                -o "$sample".t.fastq \
                "$in" &&
            echo "* cutadapt finished for " $sample
        else
            $CUTADAPT \
                -a AGATCGGAAGAGC \
                --minimum-length=20 \
                --cores="$cpus" \
                -o "$sample".t.fastq \
                "$in" &&
            echo "* cutadapt finished for " $sample
        fi


        rm $in
    else
        echo "* Skipping cutadapt step"
    fi
